# 1. Wait for database connection

for i in `seq 10` true; do
    mysqladmin ping \
        --host=mysql_drupal7 \
        --user=user \
        --password=password > /dev/null 2>&1
    result=$?
    if [ $result -eq 0 ] ; then
        break
    fi
    sleep 1
done

# 2. If site not already installed, install site and enable plugins

( ./vendor/drush/drush/drush status bootstrap | grep -q Successful ) || \
    ./vendor/drush/drush/drush -y site-install \
        --db-url=mysql://user:password@mysql_drupal7:3306/drupal \
        --account-name=admin \
        --account-pass=admin \
        standard \
        install_configure_form.update_status_module='array(FALSE,FALSE)'

./vendor/drush/drush/drush en \
    tmgmt_ui tmgmt_entity_ui tmgmt_node_ui tmgmt_transifex tmgmt_locale \
    i18n_field i18n_menu i18n_node i18n_select entity_translation_i18n_menu \
    i18n_path i18n_sync locale_translation_context tmgmt_i18n_string title \
    i18nviews views language_switcher i18n_block  --y

# 3. Run default entrypoint

docker-php-entrypoint apache2-foreground
