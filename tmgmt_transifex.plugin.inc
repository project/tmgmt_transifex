<?php

include 'utils/segment.php';

const ENDPOINT_URL = 'https://rest.api.transifex.com'; # for production
// const ENDPOINT_URL = 'http://apiv3-web:8004'; # for local

// Low-level doRequest:
//   - Applies options
//   - applies raw request body
//   - returns raw response body
function doRequest($method, $url, $options = array()) {
  $ch = curl_init($url);

  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

  if (isset($options['body'])) {
    curl_setopt($ch, CURLOPT_POSTFIELDS, $options['body']);
  }

  if (isset($options['agent'])) {
    curl_setopt($ch, CURLOPT_USERAGENT, $options['agent']);
  }

  if (isset($options['headers'])) {
    curl_setopt($ch, CURLOPT_HTTPHEADER, $options['headers']);
  }

  if (isset($options['follow'])) {
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $options['follow']);
  }

  $response = curl_exec($ch);

  $error = curl_error($ch);
  if ($error) {
    $result = array('error' => $error);
  } else {
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $result = array(
      'error' => $status < 200 || $status >= 400,
      'status' => $status,
      'redirect_url' => curl_getinfo($ch, CURLINFO_REDIRECT_URL),
      'body' => $response
    );
  }
  curl_close($ch);
  return $result;
}


class TransifexApi {
  const METADATA_NAME = "version";
  const METADATA_NAMESPACE = "__drupal__";
  const POLL_INTERVAL = 5;
  var $translator;
  
  function __construct($translator) {
    $this->translator = $translator;
  }

  // High-level doRequest:
  //   - wraps low-level doRequest
  //   - applies prefix to URL if needed
  //   - applies standard headers
  //   - encodes request body
  //   - returns decoded response body
  function doRequest($method, $url, $options = array()) {
    if (substr($url, 0, strlen(ENDPOINT_URL)) != ENDPOINT_URL) {
      $url = ENDPOINT_URL . $url;
    }
    $options['agent'] = implode(
      " ",
      array(
        "tmgmt-transifex-drupal-version/" . VERSION,
        "tmgmt-transifex-user/" . md5($this->translator->getSetting('api'))
      )
    );
    if (isset($options['body'])) {
      $options['body'] = json_encode($options['body']);
    } else {
      $options['body'] = '';
    }
    if (!isset($options['headers'])) {
      $options['headers'] = array();
    }
    $options['headers'] = array_merge($options['headers'], array(
      'Content-Type: application/vnd.api+json',
      'Content-Length: ' . strlen($options['body']),
      'Authorization: Bearer ' . $this->translator->getSetting('api')
    ));

    $response = doRequest($method, $url, $options);
    if (isset($response['body'])) {
      $response['body'] = json_decode($response['body']);
    }

    return $response;
  }

  /**
   * @param $slug
   *
   * @return mixed
   */
  function getResource($slug) {
    $response = $this->doRequest('GET', '/resources/' . $this->getResourceId($slug));
    return $response['body']->data->attributes;
  }

  function getStats($slug, $tx_language) {
    $language_id = $this->getResourceId($slug) . ':l:' . $tx_language;
    $response = $this->doRequest('GET', '/resource_language_stats/' . $language_id);
    return $response['body']->data->attributes;
  }

  /**
   * @param $slug
   * @param $tx_language
   *
   * @return mixed
   */
  function getTranslations($slug, $tx_language) {
    $response = $this->doRequest(
      'POST',
      '/resource_translations_async_downloads',
      array('body' => array('data' => array(
        'type' => 'resource_translations_async_downloads',
        'relationships' => array(
          'resource' => array('data' => array(
            'type' => 'resources',
            'id' => $this->getResourceId($slug)
          )),
          'language' => array('data' => array(
            'type' => 'languages',
            'id' => 'l:' . $tx_language
          ))
        )
      )))
    );
    $url = $response['body']->data->links->self;
    while (true) {
      sleep(self::POLL_INTERVAL);
      $response = $this->doRequest('GET', $url, array('follow' => false));
      if ($response['status'] == 303) {
        // Using low-level doRequest here
        $response = doRequest('GET', $response['redirect_url']);
        return $response['body'];
      }
      if ($response['status'] != 200) {
        throw new TMGMTException("Unable to download file");
      }
      if ($response['body']->data->attributes->status == "failed") {
        throw new TMGMTException("Unable to download file");
      }
    }
  }

  /**
   * Create a new resource in Transifex associated with the given translation
   * job id
   * @param $slug
   * @param $name
   * @param $tjid
   * @param $content
   *
   * @return mixed|string
   */
  function createResource($slug, $name, $tjid, $content) {
    $this->doRequest('POST', '/resources', array('body' => array('data' => array(
      'type' => 'resources',
      'attributes' => array(
        'name' => $name,
        'slug' => $slug,
        'categories' => array('tjid:' . $tjid)
      ),
      'relationships' => array(
        'project' => array('data' => array(
          'type' => 'projects',
          'id' => $this->getProjectId()
        )),
        'i18n_format' => array('data' => array(
          'type' => 'i18n_formats',
          'id' => 'HTML'
        ))
      )
    ))));
    $this->doRequest(
      'POST',
      '/resource_metadata',
      array('body' => array('data' => array(
        'type' => 'resource_metadata',
        'attributes' => array(
          'namespace' => self::METADATA_NAMESPACE,
          'name' => self::METADATA_NAME,
          'value' => VERSION
        ),
        'relationships' => array('resource' => array('data' => array(
          'type' => 'resources',
          'id' => $this->getResourceId($slug)
        )))
      )))
    );
    return $this->_upload_source($slug, $content);
  }

  /**
   * Update an existing resource in Transifex
   * @param $slug
   * @param $name
   * @param $categories
   * @param $content
   *
   * @return mixed|string
   */
  function updateResource($slug, $name, $tjid, $content) {
    $response = $this->doRequest('GET', '/resources/' . $this->getResourceId($slug));
    $categories = $response['body']->data->attributes->categories;
    array_push($categories, 'tjid:' . $tjid);
    $this->doRequest(
      'PATCH',
      '/resources/' . $this->getResourceId($slug),
      array('body' => array('data' => array(
        'type' => 'resources',
        'id' => $this->getResourceId($slug),
        'attributes' => array('name' => $name, 'categories' => $categories)
      )))
    );

    $metadata_id = $this->getResourceId($slug) . ':ns:' . self::METADATA_NAMESPACE . ':n:' . self::METADATA_NAME;
    $response = $this->doRequest(
      'PATCH',
      '/resource_metadata/' . $metadata_id,
      array('body' => array('data' => array(
        'type' => 'resource_metadata',
        'id' => $metadata_id,
        'attributes' => array('value' => VERSION)
      )))
    );
    if ($response['status'] == 404) {
      $this->doRequest(
        'POST',
        '/resource_metadata',
        array('body' => array('data' => array(
          'type' => 'resource_metadata',
          'attributes' => array(
            'namespace' => self::METADATA_NAMESPACE,
            'name' => self::METADATA_NAME,
            'value' => VERSION
          ),
          'relationships' => array('resource' => array('data' => array(
            'type' => 'resources',
            'id' => $this->getResourceId($slug)
          )))
        )))
      );
    }

    return $this->_upload_source($slug, $content);
  }

  function _upload_source($slug, $content) {
    $response = $this->doRequest(
      'POST',
      '/resource_strings_async_uploads',
      array('body' => array('data' => array(
        'type' => 'resource_strings_async_uploads',
        'attributes' => array(
          'content' => $content,
          'content_encoding' => 'text'
        ),
        'relationships' => array('resource' => array('data' => array(
          'type' => 'resources',
          'id' => $this->getResourceId($slug)
        ))),
      )))
      
    );
    $url = $response['body']->data->links->self;
    while (true) {
      sleep(self::POLL_INTERVAL);
      $response = $this->doRequest('GET', $url);
      if ($response['status'] != 200) {
        throw new TMGMTException("Unable to upload file");
      }
      $status = $response['body']->data->attributes->status;
      if ($status == 'failed') {
        throw new TMGMTException("Unable to upload file");
      }
      if ($status == 'succeeded') {
        return $response['body']->data->attributes->details;
      }
    }
  }


  /**
   * Create or update resource with the given slug on Transifex
   * @param $slug
   * @param $name
   * @param $tjid
   * @param $content
   *
   * @return mixed|string
   */
  function upsertResource($slug, $name, $tjid, $content) {
    $url = '/resources?filter[project]=' . $this->getProjectId() . '&filter[slug]=' . $slug;
    if (count($this->doRequest('GET', $url)['body']->data) == 0) {
      return $this->createResource($slug, $name, $tjid, $content);
    } else {
      return $this->updateResource($slug, $name, $tjid, $content);
    }
  }

  function getProjectId() {
    [$organization_slug, $project_slug] = extractSlugFromUrl(
      $this->translator->getSetting('project')
    );
    return 'o:' . $organization_slug . ':p:' . $project_slug;
  }

  function getResourceId($slug) {
    [$organization_slug, $project_slug] = extractSlugFromUrl(
      $this->translator->getSetting('project')
    );
    return 'o:' . $organization_slug . ':p:' . $project_slug . ':r:' . $slug;
  }
}

/**
 * @file
 * Provides Transifex Translator plugin controller.
 *
 */

/**
 * Transifex translator plugin controller.
 */
class TMGMTTransifexTranslatorPluginController extends TMGMTDefaultTranslatorPluginController {

  /**
   * Translation service URL.
   *
   * @var string
   */
  protected $translatorUrl = ENDPOINT_URL;

  /**
   * Maximum supported characters.
   *
   * @var int
   */
  protected $maxCharacters = 10000;

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::isAvailable().
   *
   * @param \TMGMTTranslator $translator
   *
   * @return bool
   */
  public function isAvailable(TMGMTTranslator $translator) {
    if ($translator->getSetting('api')) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @param \TMGMTJobItem $job_item
   *
   * @return array
   * The array of strings existing in the node in the form of:
   * array(
   *   array(
   *     'key' => 'node_key',
   *     'value' => 'node content'
   *   )
   * )
   */
  public function extractStringsFromNode(TMGMTJobItem $job_item) {
    $translation_payload = array();
    foreach ($job_item->data as $fieldName => $value ) {
      $propCount = 0;
      if (isset($value[$propCount])) {
        while (isset($value[$propCount])) {
          if (in_array($value[$propCount]['format']['#text'], array('full_html', 'filtered_html'))) {
            list($context, $comment) = $this->_get_context_and_comment($job_item, $fieldName);
            $translation_payload[] = array(
              'key' => $fieldName . '_' . $propCount,
              'value' => $value[$propCount]['value']['#text'],
              'context' => $context,
              'developer_comment' => $comment
            );
          } else {
            $Sentence	= new Sentence;
            list($context, $comment) = $this->_get_context_and_comment($job_item, $fieldName);
            $translation_payload[] = array(
              'key' => $fieldName . '_' . $propCount,
              'value' => $Sentence->split($value[$propCount]['value']['#text']),
              'context' => $context,
              'developer_comment' => $comment
            );
          }
          $propCount++;
        }
      } else {
          /*
          Not all job items include information about the text_format, in fact,
          most I18n String items do not. For those cases, we do a simple check
          to see if the text has html tags, and if it does, we handle it as a
          full_html text format instead of plain_text.
          */
          //Check if text has html tags. If it does, treat as full_html.
          list($context, $comment) = $this->_get_context_and_comment($job_item, $fieldName);
          if($value['#text'] != strip_tags($value['#text'])){
            $translation_payload[] = array(
              'key' => $fieldName,
              'value' => html_entity_decode($value['#text']),
              'context' => $context,
              'developer_comment' => $comment
            );
          }else{
            $Sentence = new Sentence;
            $translation_payload[] = [
              'key' => $fieldName,
              'value' => $Sentence->split($value['#text']),
              'context' => $context,
              'developer_comment' => $comment
            ];
          }
      }
    }
    return $translation_payload;
  }

  protected function _get_context_and_comment($job_item, $fieldName) {
    if ($job_item->plugin == 'locale') {
      $locale_object = $this->_getLocaleObject($job_item);
      return array($locale_object->context, $locale_object->location);
    } elseif ($job_item->plugin == 'i18n_string') {
      list(, $type, $object_id) = explode(':', $job_item->item_id, 3);
      $wrapper = tmgmt_i18n_string_get_wrapper(
        $job_item->item_type,
        (object) array('type' => $type, 'objectid' => $object_id)
      );
      $i18n_strings = $wrapper->get_strings();
      $i18n_string = NULL;
      foreach ($i18n_strings as $key => $value) {
        if ($key == $fieldName) {
          $i18n_string = $value;
          break;
        }
      }
      return array($i18n_string->context, $i18n_string->location);
    } else {
      return array('', '');
    }
  }

  protected function _getLocaleObject(TMGMTJobItem $job_item) {
    # Copied from tmgmt/sources/locale/tmgmt_locale.plugin.inc
    $locale_lid = $job_item->item_id;

    // Check existence of assigned lid.
    $exists = db_query("SELECT COUNT(lid) FROM {locales_source} WHERE lid = :lid", array(':lid' => $locale_lid))->fetchField();
    if (!$exists) {
      throw new TMGMTException(t('Unable to load locale with id %id', array('%id' => $job_item->item_id)));
    }

    // This is necessary as the method is also used in the getLabel() callback
    // and for that case the job is not available in the cart.
    if (!empty($job_item->tjid)) {
      $source_language = $job_item->getJob()->source_language;
    }
    else {
      $source_language = $job_item->getSourceLangCode();
    }

    if ($source_language == 'en') {
      $query = db_select('locales_source', 'ls');
      $query
        ->fields('ls')
        ->condition('ls.lid', $locale_lid);
      $locale_object = $query
        ->execute()
        ->fetchObject();

      $locale_object->language = 'en';

      if (empty($locale_object)) {
        return null;
      }

      $locale_object->origin = 'source';
    }
    else {
      $query = db_select('locales_target', 'lt');
      $query->join('locales_source', 'ls', 'ls.lid = lt.lid');
      $query
        ->fields('lt')
        ->fields('ls')
        ->condition('lt.lid', $locale_lid)
        ->condition('lt.language', $source_language);
      $locale_object = $query
        ->execute()
        ->fetchObject();

      if (empty($locale_object)) {
        return null;
      }

      $locale_object->origin = 'target';
    }

    return $locale_object;
  }

  public static function renderHTML($label, $text, $context, $developer_comment) {
    if (!is_array($text)) {
      $result = "<div class=\"tx_string\" id=\"" . $label . "\"";
      if ($context) {
        $result = $result . " tx-context=\"" . $context . "\"";
      }
      if ($developer_comment) {
        $result = $result . " tx-comment=\"" . $developer_comment . "\"";
      }
      $result = $result . ">" . $text . "</div>";
      return $result;

    } else {
      $result = "<div class=\"tx_string\" id=\"" . $label . "\"";
      if ($context) {
        $result = $result . " tx-context=\"" . $context . "\"";
      }
      if ($developer_comment) {
        $result = $result . " tx-comment=\"" . $developer_comment . "\"";
      }
      $result = $result . ">";

      foreach ($text as $sentence) {
        $result .= "<div class=\"tx_string_sentence\">" . nl2br($sentence) . '</div>';
      }
      return $result . "</div>";
    }
  }

  /**
   * @param $strings
   * An array of key, value strings to be encoded as an html document
   *
   * @return string
   * The final html containing all the strings as <div> elements with the
   * content in the div body and the key as the id attribute of the div
   */
  public function renderHTMLFromStrings($strings) {
    $html = '';
    $title_index = array_search(
      'title_field_0', array_column($strings, 'key')
    );
    if ($title_index) {
      $html .= $this->renderHTML(
        'title_field_0',
        $strings[$title_index]['value'],
        $strings[$title_index]['context'],
        $strings[$title_index]['developer_comment']
      );
      unset($strings[$title_index]);
    }
    // Get the rest
    foreach ($strings as $string) {
      $html .= $this->renderHTML(
        $string['key'],
        $string['value'],
        $string['context'],
        $string['developer_comment']
      );
    }
    return $html;
  }

  /**
   * Implements TMGMTTranslatorPluginControllerInterface::requestTranslation().
   *
   * @param \TMGMTJob $job
   */
  public function requestTranslation(TMGMTJob $job) {
    $translator = $job->getTranslator();
    $tx = new TransifexApi($translator);
    $job_count = 0;

    foreach ($job->getItems() as $job_item) {
      $title = $job_item->label();

      $strings = $this->extractStringsFromNode($job_item);
      $payload = $this->renderHTMLFromStrings($strings);
      $slug = slugForItem($job_item);
      $res = $tx->upsertResource($slug, $title, $job->tjid, $payload);
      drupal_set_message('Added ' . $res->strings_created . ' strings to resource named: ' . $title, 'status');
      $job_count++;
    }
    $job->submitted($job_count . ' translation job(s) have been submitted or updated to Transifex.');

    # After job is submitted check for translations because this resets the status of the job
    foreach ($job->getItems() as $job_item) {
      $slug = slugForItem($job_item);
      # See if we need to accept translations immediately
        if (shouldManualUpdateTranslations($translator, $slug, $job->target_language)) {
          watchdog(
            'tmgmt_transifex',
            'Job ' . $slug . ' -> ' . $job->target_language . ' already has translations, fetching'
          );
          tmgmt_get_translations($translator, $slug, $job->target_language, false);
        }
     }
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::getSupportedRemoteLanguages().
   *
   * @param \TMGMTTranslator $translator
   *
   * @return array
   */
  public function getSupportedRemoteLanguages(TMGMTTranslator $translator) {
    $tx = new TransifexApi($translator);
    $response = $tx->doRequest('GET', '/languages');
    $languages = array();
    if ($response['error']) {
      return $languages;
    }
    foreach ($response['body']->data as $language) {
      $languages[$language->attributes->code] = $language->attributes->code;
    }
    return $languages;
  }

  public function getTransifexProject(TMGMTTranslator $translator) {
    $tx = new TransifexApi($translator);
    return $tx->doRequest('GET', '/projects/' . $tx->getProjectId());
  }


  /**
   * Overrides TMGMTDefaultTranslatorPluginController::getDefaultRemoteLanguagesMappings().
   */
  public function getDefaultRemoteLanguagesMappings() {
    return array(
      'zh-hans' => 'zh-CHS',
      'zh-hant' => 'zh-CHT',
    );
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::getSupportedTargetLanguages().
   *
   * @param \TMGMTTranslator $translator
   * @param $source_language
   *
   * @return array
   */
  public function getSupportedTargetLanguages(TMGMTTranslator $translator, $source_language) {
    return $this->getSupportedRemoteLanguages($translator);
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::hasCheckoutSettings().
   *
   * @param \TMGMTJob $job
   *
   * @return bool
   */
  public function hasCheckoutSettings(TMGMTJob $job) {
    return FALSE;
  }

}
